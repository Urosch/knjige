<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2116</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">a0477d09-6685-4c71-b3b7-512d7e00cd41</dc:identifier>
        <dc:title>The Master and Margarita</dc:title>
        <dc:creator opf:file-as="Bulgakov, Mikhail" opf:role="aut">Mikhail Bulgakov</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2006-04-15T12:04:18.525000+00:00</dc:date>
        <dc:description>SUMMARY: Surely no stranger work exists in the annals of protest literature than The Master and Margarita. Written during the Soviet crackdown of the 1930s, when Mikhail Bulgakov's works were effectively banned, it wraps its anti-Stalinist message in a complex allegory of good and evil. Or would that be the other way around? The book's chief character is Satan, who appears in the guise of a foreigner and self-proclaimed black magician named Woland. Accompanied by a talking black tomcat and a "translator" wearing a jockey's cap and cracked pince-nez, Woland wreaks havoc throughout literary Moscow. First he predicts that the head of noted editor Berlioz will be cut off; when it is, he appropriates Berlioz's apartment. (A puzzled relative receives the following telegram: "Have just been run over by streetcar at Patriarch's Ponds funeral Friday three afternoon come Berlioz.") Woland and his minions transport one bureaucrat to Yalta, make another one disappear entirely except for his suit, and frighten several others so badly that they end up in a psychiatric hospital. In fact, it seems half of Moscow shows up in the bin, demanding to be placed in a locked cell for protection. Meanwhile, a few doors down in the hospital lives the true object of Woland's visit: the author of an unpublished novel about Pontius Pilate. This Master--as he calls himself--has been driven mad by rejection, broken not only by editors' harsh criticism of his novel but, Bulgakov suggests, by political persecution as well. Yet Pilate's story becomes a kind of parallel narrative, appearing in different forms throughout Bulgakov's novel: as a manuscript read by the Master's indefatigable love, Margarita, as a scene dreamed by the poet--and fellow lunatic--Ivan Homeless, and even as a story told by Woland himself. Since we see this narrative from so many different points of view, who is truly its author? Given that the Master's novel and this one end the same way, are they in fact the same book? These are only a few of the many questions Bulgakov provokes, in a novel that reads like a set of infinitely nested Russian dolls: inside one narrative there is another, and then another, and yet another. His devil is not only entertaining, he is necessary: "What would your good be doing if there were no evil, and what would the earth look like if shadows disappeared from it?" Unsurprisingly--in view of its frequent, scarcely disguised references to interrogation and terror--Bulgakov's masterwork was not published until 1967, almost three decades after his death. Yet one wonders if the world was really ready for this book in the late 1930s, if, indeed, we are ready for it now. Shocking, touching, and scathingly funny, it is a novel like no other. Woland may reattach heads or produce 10-ruble notes from the air, but Bulgakov proves the true magician here. The Master and Margarita is a different book each time it is opened. --Mary Park --This text refers to the Hardcover edition.</dc:description>
        <dc:publisher>Penguin Classics (2006年1月26日)</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780141188287</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Modern fiction</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Modern &amp; contemporary fiction (post c 1945)</dc:subject>
        <dc:subject>Classics</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>Classic fiction (pre c 1945)</dc:subject>
        <dc:subject>Classic fiction</dc:subject>
        <dc:subject>General &amp; Literary Fiction</dc:subject>
        <meta content="{&quot;Mikhail Bulgakov&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:03:35.467309+00:00" name="calibre:timestamp"/>
        <meta content="Master and Margarita, The" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780141188287&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
