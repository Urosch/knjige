<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3592</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">18664ef5-0b7e-4b18-b344-a47cb3499212</dc:identifier>
        <dc:title>Chest Radiology: Patterns and Differential Diagnoses, 7e</dc:title>
        <dc:creator opf:file-as="Reed, James C. Md" opf:role="aut">James C. Reed Md</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.14.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2017-12-19T23:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;Sharpen your skills in chest x-ray interpretation using this trusted clinical resource! &lt;em&gt;Chest Radiology: Patterns and Differential Diagnoses, 7th Edition,&lt;/em&gt; by Dr. James Reed, walks you through a &lt;strong&gt;logical, sequential thought process for the differential diagnoses of 23 radiologic patterns of common chest diseases&lt;/strong&gt;, using &lt;strong&gt;150 superbly illustrated patient cases&lt;/strong&gt;. You’ll gain a solid and thorough understanding of how to read and interpret chest x-rays with expert guidance on common disease patterns, differential diagnoses, narrowing down the diagnoses, and further studies (from additional radiographic exams to CT or to biopsy). Each chapter &lt;strong&gt;follows a consistent format&lt;/strong&gt;: Presenting Case, Questions, Discussion, Top 5 Diagnoses, Summary, and Answer Guide.&lt;/p&gt;
&lt;hr&gt;
&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Heavily illustrated&lt;/strong&gt; with chest radiographs and additional CT, HRCT, and MR correlative images; plus &lt;strong&gt;bulleted summary boxes&lt;/strong&gt; of comprehensive diagnoses lists and interpretation points. &lt;/li&gt;
&lt;li&gt;
&lt;p&gt;An ideal resource for &lt;strong&gt;mastering this lower-cost modality&lt;/strong&gt; before considering more complicated and costly procedures.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Illustrated case studies and quizzes feature &lt;strong&gt;new and improved questions&lt;/strong&gt; that address the challenges seen in practice today. 
**&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;strong&gt;
  * &lt;/strong&gt;Significant updates on carcinoid tumors and pulmonary nodules&lt;strong&gt;, including pathologic descriptions, detection, management, and new terminology; new lung cancer screening guidelines and benefits of CT vs. x-ray for screening; and CT description of nodules, detection of pulmonary nodules with chest x-rays, anatomic and perception problems with x-rays in identifying nodules.
&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;
  * &lt;/strong&gt;New content on diffuse disease&lt;strong&gt;, including new pathologic terms and the latest impact of high resolution CT (HRCT).
&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;
  * &lt;/strong&gt;Updates on radiation dosing&lt;strong&gt; and emphasis on lower dosing, and an expanded emphasis on how to interpret x-rays and CT scans.
&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;
  * &lt;/strong&gt;Expert Consult™ eBook version included with purchase.&lt;strong&gt; This &lt;/strong&gt;enhanced eBook experience** allows you to search all of the text, figures, and references from the book on a variety of devices.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Elsevier</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780323498319</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">0323498310</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;James C. Reed Md&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2018-01-07T11:38:15.890267+00:00" name="calibre:timestamp"/>
        <meta content="Chest Radiology: Patterns and Differential Diagnoses, 7e" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_editable&quot;: true, &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_csp&quot;: false, &quot;is_multiple&quot;: null, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;kind&quot;: &quot;field&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;colnum&quot;: 1, &quot;#extra#&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;contains_html&quot;: false, &quot;make_category&quot;: false, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;use_decorations&quot;: 0}, &quot;label&quot;: &quot;isbn&quot;, &quot;#value#&quot;: &quot;9780323498319&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;category_sort&quot;: &quot;value&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
