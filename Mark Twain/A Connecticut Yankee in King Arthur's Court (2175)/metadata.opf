<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2175</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">8f4b305f-1a63-4b94-8aaa-2e49d654040a</dc:identifier>
        <dc:title>A Connecticut Yankee in King Arthur's Court</dc:title>
        <dc:creator opf:file-as="Twain, Mark" opf:role="aut">Mark Twain</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2005-09-01T07:00:00+00:00</dc:date>
        <dc:description>SUMMARY: A Connecticut Yankee in King Arthur's Court, by Mark Twain, is part of the Barnes &amp; Noble Classics series, which offers quality editions at affordable prices to the student and the general reader, including new scholarship, thoughtful design, and pages of carefully crafted extras. Here are some of the remarkable features of Barnes &amp; Noble Classics: New introductions commissioned from today's top writers and scholars Biographies of the authors Chronologies of contemporary historical, biographical, and cultural events Footnotes and endnotes Selective discussions of imitations, parodies, poems, books, plays, paintings, operas, statuary, and films inspired by the work Comments by other famous authors Study questions to challenge the reader's viewpoints and expectations Bibliographies for further reading Indices &amp; Glossaries, when appropriateAll editions are beautifully designed and are printed to superior specifications; some include illustrations of historical interest. Barnes &amp; Noble Classics pulls together a constellation of influences—biographical, historical, and literary—to enrich each reader's understanding of these enduring works. One of the greatest satires in American literature, Mark Twain’s A Connecticut Yankee in King Arthur’s Court begins when Hank Morgan, a skilled mechanic in a nineteenth-century New England arms factory, is struck on the head during a quarrel and awakens to find himself among the knights and magicians of King Arthur’s Camelot.What follows is a culture clash of the first magnitude, as practical-minded Hank, disgusted with the ignorance and superstition of the people, decides to enlighten them with education and technology. Through a series of wonderfully imaginative adventures, Twain celebrates American homespun ingenuity and democracy as compared to the backward ineptitude of a chivalric monarchy. At the same time, however, Twain raises the question of whether material progress necessarily creates a better society. As Hank becomes more powerful and self-righteous, he also becomes more ruthless, more autocratic, and less able to control events, until the only way out is a massively destructive war.While the dark pessimism that would fully blossom in Twain’s later works can be discerned in A Connecticut Yankee in King Arthur’s Court, the novel will nevertheless be remembered primarily for its wild leaps of imagination, brilliant wit, and entertaining storytelling.With over 200 of the original illustrations by Dan Beard. Stephen Railton teaches American literature at the University of Virginia. His most recent book is Mark Twain: A Short Introduction.</dc:description>
        <dc:publisher>BARNES &amp; NOBLE</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781593082109</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Literary</dc:subject>
        <dc:subject>Americans</dc:subject>
        <dc:subject>Magic</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Time travel</dc:subject>
        <dc:subject>Classics</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>Literature: Classics</dc:subject>
        <dc:subject>Travel</dc:subject>
        <dc:subject>Fantasy</dc:subject>
        <dc:subject>Great Britain</dc:subject>
        <dc:subject>Science Fiction</dc:subject>
        <dc:subject>Juvenile Fiction</dc:subject>
        <dc:subject>Literature - Classics</dc:subject>
        <dc:subject>Criticism</dc:subject>
        <meta content="{&quot;Mark Twain&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:03:38.572626+00:00" name="calibre:timestamp"/>
        <meta content="Connecticut Yankee in King Arthur's Court, A" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9781593082109&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
