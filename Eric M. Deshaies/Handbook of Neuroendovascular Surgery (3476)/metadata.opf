<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3476</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">209cbb88-21b3-4c87-9351-325bb105defd</dc:identifier>
        <dc:title>Handbook of Neuroendovascular Surgery</dc:title>
        <dc:creator opf:file-as="Deshaies, Eric M. &amp; Eddleman, Christopher S. &amp; Boulos, Alan S." opf:role="aut">Eric M. Deshaies</dc:creator>
        <dc:creator opf:file-as="Deshaies, Eric M. &amp; Eddleman, Christopher S. &amp; Boulos, Alan S." opf:role="aut">Christopher S. Eddleman</dc:creator>
        <dc:creator opf:file-as="Deshaies, Eric M. &amp; Eddleman, Christopher S. &amp; Boulos, Alan S." opf:role="aut">Alan S. Boulos</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.10.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2011-10-10T22:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;&lt;em&gt;The authors set out to produce a conglomerate of rapid facts and technical and clinical pearls for operators preparing for cases, and in this they are eminently successful. The book is a valuable resource, and an excellent icebreaker for pocket handbooks for our field. -- &lt;strong&gt;American Journal of Neuroradiology&lt;/strong&gt;&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;Ideal for both newcomers and practitioners of the specialty, &lt;em&gt;Handbook of Neuroendovascular Surgery&lt;/em&gt; is both a succinct introduction and a quick reference guide for key concepts and technical information prior to, during, and after a procedure. It progresses logically from basic scientific concepts to equipment and technical aspects to treatment of specific neurovascular diseases, expertly capturing the core information needed in daily practice.&lt;/p&gt;
&lt;p&gt;Key Features:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;Contributions by neurosurgeons, radiologists, and neurologists reflect the multidisciplinary nature of neuroendovascular treatment   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Critical summaries of Peri-Procedural Patient Care and Equipment and Techniques to help in case preparation   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Generous use of tables and illustrations create fast visual summaries and distill large amounts of information   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Valuable appendices on routinely used technical information, pathology classification systems, endovascular medications, and full-color pictorials designed for teaching and patient education. The pictorials are available for download at Thieme's Media Center.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Written by specialists trained in both open cerebrovascular neurosurgery and neuroendovascular surgery, this portable handbook is a treasure trove of practical information that is essential for both beginners and more experienced neurosurgeons who want to refresh their knowledge in state-of-the-art neuroendovascular techniques.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Thieme</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781604063004</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">1604063009</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Christopher S. Eddleman&quot;: &quot;&quot;, &quot;Alan S. Boulos&quot;: &quot;&quot;, &quot;Eric M. Deshaies&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="Thieme Flexibooks" name="calibre:series"/>
        <meta content="1" name="calibre:series_index"/>
        <meta content="8" name="calibre:rating"/>
        <meta content="2017-10-25T18:56:48.862738+00:00" name="calibre:timestamp"/>
        <meta content="Handbook of Neuroendovascular Surgery" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;rec_index&quot;: 22, &quot;column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;datatype&quot;: &quot;composite&quot;, &quot;is_custom&quot;: true, &quot;kind&quot;: &quot;field&quot;, &quot;#extra#&quot;: null, &quot;is_category&quot;: false, &quot;display&quot;: {&quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;description&quot;: &quot;&quot;, &quot;use_decorations&quot;: 0, &quot;make_category&quot;: false, &quot;contains_html&quot;: false}, &quot;#value#&quot;: &quot;9781604063004&quot;, &quot;is_multiple&quot;: null, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_editable&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
