<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2648</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">afe0f0be-b735-4242-b9c5-b197c0dd3194</dc:identifier>
        <dc:title>Theodore Rex</dc:title>
        <dc:creator opf:file-as="Morris, Edmund" opf:role="aut">Edmund Morris</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2002-10-01T10:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;h3&gt;Amazon.com Review&lt;/h3&gt;&lt;p&gt;In this lively biography, Edmund Morris returns to the gifted, energetic, and thoroughly controversial man whom the novelist Henry James called "King Theodore." In his two terms as president of the United States, Roosevelt forged an American empire, and he behaved as if it was his destiny. In this sequel to his Pulitzer Prize-winning biography &lt;em&gt;The Rise of Theodore Roosevelt&lt;/em&gt;, Morris charts Roosevelt's accomplishments: the acquisition of the Panama Canal and the Philippines, the creation of national parks and monuments, and more. "Collaring Capital and Labor in either hand," Morris writes, Roosevelt made few friends, but he usually got what he wanted--and earned an enduring place in history.&lt;/p&gt;&lt;p&gt;Morris combines a fine command of the era's big issues with an appreciation for the daily minutiae involved in governing a nation. Less controversially inventive, but no less readable, than the Ronald Reagan biography &lt;em&gt;Dutch&lt;/em&gt;, &lt;em&gt;Theodore Rex&lt;/em&gt; gives readers new reason both to admire and fault an American phenomenon. &lt;em&gt;--Gregory McNamee&lt;/em&gt;&lt;/p&gt;&lt;h3&gt;From Publishers Weekly&lt;/h3&gt;&lt;p&gt;The second entry in Morris's projected three-volume life of Theodore Roosevelt focuses on the presidential years 1901 through early 1909. Impeccably researched and beautifully composed, Morris's book provides what is arguably the best consideration of Roosevelt's presidency ever penned. Making good use of TR's private and presidential papers as well as the archives of such prot‚g‚s as John Hay, William Howard Taft, Owen Wister and John Burroughs Morris marshals a rich array of carefully chosen and beautifully rendered vignettes to create a dazzling portrait of the man (the youngest ever to hold the office of president). Morris proves the perfect guide through TR's eight breathless, fertile years in the White House: years during which the doting father and prolific author conserved millions of Western acres, swung his "big stick" at trusts and monopolies, advanced progressive agendas on race and labor relations, fostered a revolution in Panama (where he sought to build his canal), won the Nobel Peace Prize for mediating an end to the Russo-Japanese War and pushed through the Pure Food and Drug Act. John Burroughs once wrote that the hypercreative TR "was a many sided man, and every side was like an electric battery." In the end, Morris succeeds brilliantly at capturing all of TR's many energized sides, producing a book that is every bit as complex, engaging and invigorating as the vibrant president it depicts. Illus. (On-sale: Nov. 20)Forecast: Long-awaited, this volume comes out in the centennial of TR's rise to the presidency. Morris's gift for storytelling and his outstanding reputation from volume one (and perhaps his notoriety for the controversial Reagan bio Dutch) should guarantee large sales. &lt;/p&gt;&lt;p&gt;Copyright 2001 Cahners Business Information, Inc.&lt;/p&gt; &lt;/div&gt;</dc:description>
        <dc:publisher>Random House Publishing Group</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780812966008</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Presidents</dc:subject>
        <dc:subject>Political Science</dc:subject>
        <dc:subject>20th Century</dc:subject>
        <dc:subject>Biography &amp; Autobiography</dc:subject>
        <dc:subject>History</dc:subject>
        <dc:subject>Biography</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Presidents &amp; Heads of State</dc:subject>
        <dc:subject>Historical</dc:subject>
        <dc:subject>United States</dc:subject>
        <meta content="{&quot;Edmund Morris&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:04:07+00:00" name="calibre:timestamp"/>
        <meta content="Theodore Rex" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780812966008&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
