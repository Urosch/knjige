<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">205</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">19924c8e-5b59-4c8e-862e-bff477052094</dc:identifier>
        <dc:title>Oxford Cases in Medicine and Surgery</dc:title>
        <dc:creator opf:file-as="Farne, Hugo &amp; Norris-Cervetto, Edward &amp; Warbrick-Smith, James" opf:role="aut">Hugo Farne</dc:creator>
        <dc:creator opf:file-as="Farne, Hugo &amp; Norris-Cervetto, Edward &amp; Warbrick-Smith, James" opf:role="aut">Edward Norris-Cervetto</dc:creator>
        <dc:creator opf:file-as="Farne, Hugo &amp; Norris-Cervetto, Edward &amp; Warbrick-Smith, James" opf:role="aut">James Warbrick-Smith</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2010-01-07T10:36:08+00:00</dc:date>
        <dc:description>&lt;p&gt;Case books are increasingly popular with students but most assume that the student is capable of gathering all the necessary information and making the correct diagnosis when faced with an unfamiliar clinical problem. However, even if you know how a typical 'myocardial infarction' presents, do you know how to approach a patient presenting simply with 'chest pain'?   &lt;/p&gt;
&lt;p&gt;Oxford Cases in Medicine and Surgery teaches students a hypothesis-driven, step-by-step, logical diagnostic approach to common patient presentations. This hands-on approach, which stimulates active learning, mirrors that used by successful clinicians on the wards, challenging students with questions at each stage of a case (history-taking, examination, investigation, management). In tackling these questions, students learn to integrate their existing knowledge and apply it to a real-life scenario from start to finish.   &lt;/p&gt;
&lt;p&gt;Each chapter focuses on a common presenting symptom (e.g. chest pain), rather than a physiological system (e.g. cardiology). By starting with a symptom, as doctors do in reality, students learn to draw on their knowledge of different physiological systems - for example, cardiology, respiratory, gastroenterology - at the same time.   &lt;/p&gt;
&lt;p&gt;Within each chapter, a long case walks the student through a logical method for tackling patients presenting with a given symptom. Subsequent short cases test this learning, whilst reminding students to be wary of other conditions that may present in a similar way. All the major presenting symptoms in general medicine and surgery are covered, together with a broad range of pathologies.   &lt;/p&gt;
&lt;p&gt;With references to landmark trials, relevant guidelines, and the inclusion of questions that are frequently asked in clinical settings, this book is an essential resource for all medicine students, and provides a modern, well-rounded introduction to life on the wards.  &lt;/p&gt;
&lt;p&gt;Online Resource Centre&lt;br /&gt;
The Online Resource Centre to accompany Oxford Cases in Medicine and Surgery features:  &lt;/p&gt;
&lt;p&gt;DT Figures from the book in electronic format, ready to download.&lt;br /&gt;
DT 5 Single Best Answer questions for every chapter. &lt;br /&gt;
DT Topical updates: key updates on topics in the book, to keep you up-to-date with latest developments in the field. &lt;br /&gt;
DT Hyperlinked bibliography: online links to articles referenced in the book, providing ready access to the primary literature.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;</dc:description>
        <dc:publisher>OUP Oxford</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780199560523</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">UAH-vYXTBfUC</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">0199560528</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Diagnosis</dc:subject>
        <dc:subject>Surgery</dc:subject>
        <dc:subject>Test Preparation &amp; Review</dc:subject>
        <meta content="{&quot;Hugo Farne&quot;: &quot;&quot;, &quot;Edward Norris-Cervetto&quot;: &quot;&quot;, &quot;James Warbrick-Smith&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="10" name="calibre:rating"/>
        <meta content="2017-08-09T17:48:28+00:00" name="calibre:timestamp"/>
        <meta content="Oxford Cases in Medicine and Surgery" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;#value#&quot;: &quot;9780199560523&quot;, &quot;is_multiple2&quot;: {}, &quot;colnum&quot;: 1, &quot;label&quot;: &quot;isbn&quot;, &quot;datatype&quot;: &quot;composite&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;is_csp&quot;: false, &quot;is_custom&quot;: true, &quot;kind&quot;: &quot;field&quot;, &quot;is_editable&quot;: true, &quot;display&quot;: {&quot;contains_html&quot;: false, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;composite_sort&quot;: &quot;text&quot;, &quot;description&quot;: &quot;&quot;}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple&quot;: null, &quot;rec_index&quot;: 22, &quot;is_category&quot;: false, &quot;table&quot;: &quot;custom_column_1&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
