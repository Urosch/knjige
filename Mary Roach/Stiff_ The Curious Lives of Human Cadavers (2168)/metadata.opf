<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2168</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">3408ecff-44f2-4cd0-aca5-aaaf7b73d350</dc:identifier>
        <dc:title>Stiff: The Curious Lives of Human Cadavers</dc:title>
        <dc:creator opf:file-as="Roach, Mary" opf:role="aut">Mary Roach</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2004-04-26T05:00:00+00:00</dc:date>
        <dc:description>EDITORIAL REVIEW:

**"One of the funniest and most unusual books of the year....Gross, educational, and unexpectedly sidesplitting."—*Entertainment Weekly***    

*Stiff* is an oddly compelling, often hilarious exploration of the strange lives   of our bodies postmortem. For two thousand years, cadavers—some willingly, some unwittingly—have been involved in science's boldest strides and weirdest undertakings. They've tested France's first guillotines, ridden the NASA Space Shuttle, been crucified in a Parisian laboratory to test the authenticity of the Shroud of Turin, and helped solve the mystery of TWA Flight 800. For every new surgical procedure, from heart transplants to gender reassignment surgery, cadavers have been there alongside surgeons, making history in their quiet way.    

In this fascinating, ennobling account, Mary Roach visits the good deeds of cadavers over the centuries—from the anatomy labs and human-sourced pharmacies of medieval and nineteenth-century Europe to a human decay research facility in Tennessee, to a plastic surgery practice lab, to a Scandinavian funeral directors' conference on human composting. In her droll, inimitable voice, Roach tells the engrossing story of our bodies when we are no longer with them.</dc:description>
        <dc:publisher>W. W. Norton &amp; Company</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780393324822</dc:identifier>
        <dc:language>en</dc:language>
        <dc:subject>Human dissection</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>Ethics</dc:subject>
        <dc:subject>Human experimentation in medicine</dc:subject>
        <dc:subject>Medical</dc:subject>
        <dc:subject>History</dc:subject>
        <dc:subject>History: specific events &amp; topics</dc:subject>
        <dc:subject>Dead</dc:subject>
        <dc:subject>Forensic Medicine</dc:subject>
        <dc:subject>Social Science</dc:subject>
        <dc:subject>Social Studies: General</dc:subject>
        <dc:subject>Medicine (Specific Aspects)</dc:subject>
        <dc:subject>Science: general issues</dc:subject>
        <dc:subject>Sociology: death &amp; dying</dc:subject>
        <dc:subject>Research</dc:subject>
        <dc:subject>Death &amp; Dying</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Medical Research</dc:subject>
        <dc:subject>Science</dc:subject>
        <meta content="{&quot;Mary Roach&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:03:38.190300+00:00" name="calibre:timestamp"/>
        <meta content="Stiff: The Curious Lives of Human Cadavers" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780393324822&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
