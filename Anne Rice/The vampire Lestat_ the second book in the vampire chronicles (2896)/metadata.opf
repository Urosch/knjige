<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2896</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">98d7d6f9-895c-45eb-bb7c-8b83f6f54aed</dc:identifier>
        <dc:title>The vampire Lestat: the second book in the vampire chronicles</dc:title>
        <dc:creator opf:file-as="Rice, Anne" opf:role="aut">Anne Rice</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2001-07-15T17:40:43.854000+00:00</dc:date>
        <dc:description>EDITORIAL REVIEW: Returning to the hypnotic world she so brilliantly created in **Interview with the Vampire**, Anne Rice demonstrates once again her power to enthrall. With the same richness of drama, atmosphere and incident, she tells the fantastic story of the vampire Lestat, whom we first perceived as the seductive devil-vampire of **Interview with the Vampire** and whom we now follow through the ages as he searches for the origin and meaning of his own dark immortality. And who, more and more, engages our sympathy until he stands revealed as a questing romantic, a vampire-hero with his own strange and passionate courage and morality.As the novel opens, Lestat, having risen from the earth after a fifty-five years' sleep, and infatuated with the modern world, presents himself in all his vampire brilliance as a rock star, a superstar, a seducer of millions. And, in this blaze of adulation, daring to break the vampire oath of silence, he determines to tell his story, to rouse the generations of the living dead from their slumbers and to penetrate the riddle of his own existence.As he speaks we are plunged back into eighteenth-century France, into the castle where we meet the young Lestat: child of impoverished aristocrats, heroic hunter of wolves, at odds with his tyrannical father, running away to join a traveling troupe of actors. We see him in the licentious Paris of the day, first apprentice at a boulevard theater, then its most celebrated actor, idolized, adored by many and--night after night--watched by one . . . until, in a sleep filled with dreams of the wolves he killed as a boy, he is shocked awake by a dark figure and suddenly, horribly, eternally joined to the unholy brotherhood.We follow Lestat as he searches for others like him--in churches and brothels, in gambling houses, huts and palaces--sometimes joined by the vampire-angel Gabrielle, who is bound to him both by blood and by passion; sometimes traveling with his adored Nicolas, the violinist whose music and beauty are equally transcendent. We follow Lestat as he travels from the snowcapped mountains of the Auvergne and the primeval forest of ancient Gaul to Sicily, Istanbul, Venice and Cairo, searching for his origins, sometimes finding clues to the birth of the vampire race, knowing always that the central truth eludes him.But all the while, throughout his travels, through many lands and many times, Lestat has made enemies among his brethren--vampires who are in terror of his questions, who fear he will disturb the uneasy balance in which they exist with the mortal world, and who suspect in him a desire to rule. And when, in the caves below a craggy Greek island, in a sanctuary whose walls are covered with gold-flecked murals, the very first of the living dead awake, the truth at the heart of his quest is at last revealed. Ancient forces held immobile through the ages are irreversibly set in motion, and as the novel rushes to its stunning climax, Lestat's vampire foes converge in pursuit of him on the demonic freeways of the twentieth century.</dc:description>
        <dc:publisher>Knopf</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780394534435</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>Modern fiction</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Horror - General</dc:subject>
        <dc:subject>Horror tales</dc:subject>
        <dc:subject>Horror fiction</dc:subject>
        <dc:subject>Vampires</dc:subject>
        <dc:subject>Horror</dc:subject>
        <dc:subject>Lestat (Fictitious character)</dc:subject>
        <dc:subject>General &amp; Literary Fiction</dc:subject>
        <dc:subject>Fiction - Horror</dc:subject>
        <dc:subject>Horror &amp; Ghost Stories</dc:subject>
        <meta content="{&quot;Anne Rice&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:04:23.298986+00:00" name="calibre:timestamp"/>
        <meta content="vampire Lestat: the second book in the vampire chronicles, The" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780394534435&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
