<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2781</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">9b901ece-a916-4a86-8f77-e0184f8e61b3</dc:identifier>
        <dc:title>Haunted: a novel of stories</dc:title>
        <dc:creator opf:file-as="Palahniuk, Chuck" opf:role="aut">Chuck Palahniuk</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2005-05-03T07:00:00+00:00</dc:date>
        <dc:description>EDITORIAL REVIEW: *Haunted* by Chuck Palahniuk is a novel made up of stories: Twenty-three of them, to be precise. Twenty-three of the most horrifying, hilarious, mind-blowing, stomach-churning tales you’ll ever encounter—sometimes all at once. They are told by people who have answered an ad headlined “Writers’ Retreat: Abandon Your Life for Three Months,” and who are led to believe that here they will leave behind all the distractions of “real life” that are keeping them from creating the masterpiece that is in them. But “here” turns out to be a cavernous and ornate old theater where they are utterly isolated from the outside world—and where heat and power and, most important, food are in increasingly short supply. And the more desperate the circumstances become, the more extreme the stories they tell—and the more devious their machinations become to make themselves the hero of the inevitable play/movie/nonfiction blockbuster that will surely be made from their plight.*Haunted* is on one level a satire of reality television—*The Real World* meets *Alive*. It draws from a great literary tradition—*The Canterbury Tales*, *The Decameron*, the English storytellers in the Villa Diodati who produced, among other works, *Frankenstein*—to tell an utterly contemporary tale of people desperate that their story be told at any cost. Appallingly entertaining, *Haunted* is Chuck Palahniuk at his finest—which means his most extreme and his most provocative.</dc:description>
        <dc:publisher>Random House, Inc.</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780385509480</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Horror fiction</dc:subject>
        <dc:subject>Popular American Fiction</dc:subject>
        <dc:subject>Social Science</dc:subject>
        <dc:subject>Social isolation</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Short Stories (single author)</dc:subject>
        <dc:subject>Penology</dc:subject>
        <dc:subject>Prisoners</dc:subject>
        <dc:subject>Torture victims</dc:subject>
        <dc:subject>Artists</dc:subject>
        <dc:subject>Horror</dc:subject>
        <dc:subject>Fiction - Horror</dc:subject>
        <dc:subject>Horror tales</dc:subject>
        <dc:subject>Horror - General</dc:subject>
        <meta content="{&quot;Chuck Palahniuk&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:04:15.563052+00:00" name="calibre:timestamp"/>
        <meta content="Haunted: a novel of stories" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780385509480&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
