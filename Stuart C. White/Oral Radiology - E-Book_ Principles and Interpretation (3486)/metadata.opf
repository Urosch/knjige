<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3486</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">b46c33ff-fbcd-449c-8735-e03abe6d8ef0</dc:identifier>
        <dc:title>Oral Radiology - E-Book: Principles and Interpretation</dc:title>
        <dc:creator opf:file-as="White, Stuart C. &amp; Pharoah, Michael J." opf:role="aut">Stuart C. White</dc:creator>
        <dc:creator opf:file-as="White, Stuart C. &amp; Pharoah, Michael J." opf:role="aut">Michael J. Pharoah</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.10.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2008-09-24T22:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;With more than 1,000 high-quality radiographs and illustrations, this bestselling book visually demonstrates the basic principles of oral and maxillofacial radiology as well as effective clinical application. You’ll be able to diagnose and treat patients effectively with the coverage of imaging techniques, including specialized techniques such as MRI and CT, and the comprehensive discussion of the radiographic interpretation of pathology. The book also covers radiation physics, radiation biology, and radiation safety and protection — helping you provide state-of-the-art care!&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;A consistent format makes it easy to follow and comprehend clinical material on each pathologic condition, including a definition, synonyms, clinical features, radiographic features, differential diagnosis, and management/treatment.&lt;/li&gt;
&lt;li&gt;Updated photos show new equipment and radiographs in the areas of intraoral radiographs, normal radiographic anatomy, panoramic imaging, and advanced imaging.&lt;/li&gt;
&lt;li&gt;Updated Digital Imaging chapter expands coverage of PSP plates and its use in cephalometric and panoramic imaging, examining the larger latitudes of photostimulable phosphor receptors and their linear response to the five orders of magnitude of x-ray exposure.&lt;/li&gt;
&lt;li&gt;Updated Guidelines for Prescribing Dental Radiographs chapter includes the latest ADA guidelines, and also discusses the European Guidelines.&lt;/li&gt;
&lt;li&gt;Updated information on radiographic manifestations of diseases in the orofacial region includes the latest data on etiology and diagnosis, with an emphasis on advanced imaging.&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Expert contributors include many authors with worldwide reputations.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Cone Beam Computed Tomography chapter covers machines, the imaging process, and typical clinical applications of cone-beam imaging, with examples of examinations made from scans.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;Evolve website adds more coverage of cases, with more examples of specific issues.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;**&lt;/p&gt;&lt;h3&gt;Review&lt;/h3&gt;
&lt;p&gt;"The special attractiveness of this book comes from the pleasing combination of well-structured and simple explanations complemented by more than 1000 high-quality radiographs and illustrations which cover more or less the whole area of oral and maxillofacial radiology... Summa summarum, this book can be strongly recommended to the personal library of both students and clinicians." &lt;strong&gt;European Journal of Orthodontics, 2009&lt;/strong&gt;&lt;/p&gt;
&lt;h3&gt;About the Author&lt;/h3&gt;
&lt;p&gt;Stuart C. White, DDS, PhD, Professor and Chairman, Section of Oral Radiology, University of California, School of Dentistry, Center for Health Sciences, Los Angeles, CA; and Michael J. Pharoah, DDS, University of Toronto, Toronto, Ontario &lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Mosby</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780323049832</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">B005C3QYNM</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Stuart C. White&quot;: &quot;&quot;, &quot;Michael J. Pharoah&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="8" name="calibre:rating"/>
        <meta content="2017-10-25T19:11:14.256308+00:00" name="calibre:timestamp"/>
        <meta content="Oral Radiology - E-Book: Principles and Interpretation" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;rec_index&quot;: 22, &quot;column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;datatype&quot;: &quot;composite&quot;, &quot;is_custom&quot;: true, &quot;kind&quot;: &quot;field&quot;, &quot;#extra#&quot;: null, &quot;is_category&quot;: false, &quot;display&quot;: {&quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;description&quot;: &quot;&quot;, &quot;use_decorations&quot;: 0, &quot;make_category&quot;: false, &quot;contains_html&quot;: false}, &quot;#value#&quot;: &quot;9780323049832&quot;, &quot;is_multiple&quot;: null, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_editable&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
