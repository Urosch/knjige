<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2072</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">4335bac9-a1be-4153-87f1-f3f72fc1bede</dc:identifier>
        <dc:title>The Phantom Tollbooth</dc:title>
        <dc:creator opf:file-as="Juster, Norton" opf:role="aut">Norton Juster</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>1961-08-11T10:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;h3&gt;Amazon.com Review&lt;/h3&gt;&lt;p&gt;"It seems to me that almost everything is a waste of time," Milo laments. "[T]here's nothing for me to do, nowhere I'd care to go, and hardly anything worth seeing." This bored, &lt;em&gt;bored&lt;/em&gt; young protagonist who can't see the point to anything is knocked out of his glum humdrum by the sudden and curious appearance of a tollbooth in his bedroom. Since Milo has absolutely nothing better to do, he dusts off his toy car, pays the toll, and drives through. What ensues is a journey of mythic proportions, during which Milo encounters countless odd characters who are anything but dull.&lt;/p&gt;&lt;p&gt;Norton Juster received (and continues to receive) enormous praise for this original, witty, and oftentimes hilarious novel, first published in 1961. In an introductory "Appreciation" written by &lt;em&gt;The Phantom Tollbooth&lt;/em&gt; leaps, soars, and abounds in right notes all over the place, as any proper masterpiece must." Indeed.&lt;/p&gt;&lt;p&gt;As Milo heads toward Dictionopolis he meets with the Whether Man ("for after all it's more important to know whether there will be weather than what the weather will be"), passes through The Doldrums (populated by Lethargarians), and picks up a watchdog named Tock (who has a giant alarm clock for a body). The brilliant satire and double entendre intensifies in the Word Market, where after a brief scuffle with Officer Short Shrift, Milo and Tock set off toward the Mountains of Ignorance to rescue the twin Princesses, Rhyme and Reason. Anyone with an appreciation for language, irony, or &lt;em&gt;(Ages 8 and up)&lt;/em&gt;&lt;/p&gt;&lt;h3&gt;Review&lt;/h3&gt;&lt;p&gt;" I read [The Phantom Tollbooth] first when I was 10. I still have the book report I wrote, which began 'This is the best book ever.'"&lt;br&gt;--Anna Quindlen, &lt;em&gt;The New York Times&lt;/em&gt; &lt;/p&gt;&lt;p&gt;"A classic... Humorous, full of warmth and real invention."&lt;br&gt;--_The New Yorker_ &lt;/p&gt;&lt;p&gt;&lt;em&gt;From the Trade Paperback edition.&lt;/em&gt; -- &lt;em&gt;Review&lt;/em&gt;&lt;/p&gt; &lt;/div&gt;</dc:description>
        <dc:publisher>Random House</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9780394815008</dc:identifier>
        <dc:identifier opf:scheme="MOBI-ASIN">deacf065-7f81-4cff-abcd-e4a678ce8bd7</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Dogs</dc:subject>
        <dc:subject>Classics</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>American</dc:subject>
        <dc:subject>Boys</dc:subject>
        <dc:subject>Welsh</dc:subject>
        <dc:subject>Adventure stories</dc:subject>
        <dc:subject>Drama</dc:subject>
        <dc:subject>Juvenile Fiction</dc:subject>
        <dc:subject>Fantasy</dc:subject>
        <dc:subject>Irish</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Scottish</dc:subject>
        <dc:subject>Action &amp; Adventure</dc:subject>
        <dc:subject>Imaginary places</dc:subject>
        <dc:subject>Humorous stories</dc:subject>
        <dc:subject>Children's stories</dc:subject>
        <dc:subject>Fantasy fiction</dc:subject>
        <dc:subject>Fantasy &amp; Magic</dc:subject>
        <dc:subject>English</dc:subject>
        <dc:subject>Animals</dc:subject>
        <meta content="{&quot;Norton Juster&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:03:32.385331+00:00" name="calibre:timestamp"/>
        <meta content="Phantom Tollbooth, The" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9780394815008&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
