<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3470</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">783e969d-2413-4d85-9681-2f0fdcb08616</dc:identifier>
        <dc:title>Cranial Nerves: Anatomy, Pathology, Imaging</dc:title>
        <dc:creator opf:file-as="Binder, Devin K. &amp; Sonne, D. Christian &amp; Fischbein, Nancy J." opf:role="aut">Devin K. Binder</dc:creator>
        <dc:creator opf:file-as="Binder, Devin K. &amp; Sonne, D. Christian &amp; Fischbein, Nancy J." opf:role="aut">D. Christian Sonne</dc:creator>
        <dc:creator opf:file-as="Binder, Devin K. &amp; Sonne, D. Christian &amp; Fischbein, Nancy J." opf:role="aut">Nancy J. Fischbein</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.10.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2010-04-11T22:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;&lt;em&gt;"Unique...provid[es] clear, concise descriptions...the first of its kind to offer a detailed look at the imaging findings of each cranial nerve in both normal and pathological states."--Journal of Neurosurgery&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;"This book reaches its objective. It must be part of the library of the neurological surgery student as a useful tool for understanding basic anatomy and physiology, as well as the most common pathologies and the basic neuroradiology of the cranial nerves. We strongly recommend it."-- World Neurosurgery&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;"This book is of interest to everyone who aims a solid understanding of the cranial nerves." --Central European Neurosurgery&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;This beautifully illustrated book combines a detailed exposition of the anatomy and function of the cranial nerves with practical coverage of clinical concepts for the assessment and differential diagnosis of cranial nerve dysfunction. An introductory chapter provides a brief overview of cranial nerve anatomy and function, skull base anatomy, classification of pathologies, and imaging approaches. Each of the twelve chapters that follow is devoted to in-depth coverage of a different cranial nerve. These chapters open with detailed discussion of the various functions of each nerve and normal anatomy. The authors then describe common lesions and present a series of cases that are complemented by CT images and MRIs to illustrate disease entities that result in cranial nerve dysfunction.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;Concise descriptions in a bulleted outline format enable rapid reading and review   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Tables synthesize key information related to anatomy, function, pathology, and imaging   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;More than 300 high-quality illustrations and state-of-the-art CT and MR images demonstrate important anatomic concepts and pathologic findings   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Pearls emphasize clinical information and key imaging findings for diagnosis and treatment   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Appendices include detailed information on brainstem anatomy, pupil and eye movement control, parasympathetic ganglia, and cranial nerve reflexes&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;This book is an indispensable reference for practicing physicians and trainees in neurosurgery, neurology, neuroradiology, radiology, and otolaryngology-head and neck surgery. It will also serve as a valuable resource for students seeking to gain a solid understanding of the anatomy, function, and pathology of the cranial nerves.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Thieme</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781588904027</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">kKJiCNmAu5QC</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">1588904024</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>Neuroscience</dc:subject>
        <dc:subject>Pathophysiology</dc:subject>
        <dc:subject>Surgery</dc:subject>
        <dc:subject>Neurosurgery</dc:subject>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;D. Christian Sonne&quot;: &quot;&quot;, &quot;Devin K. Binder&quot;: &quot;&quot;, &quot;Nancy J. Fischbein&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="Thieme Flexibooks" name="calibre:series"/>
        <meta content="1" name="calibre:series_index"/>
        <meta content="10" name="calibre:rating"/>
        <meta content="2017-10-25T18:34:50.806572+00:00" name="calibre:timestamp"/>
        <meta content="Cranial Nerves: Anatomy, Pathology, Imaging" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;rec_index&quot;: 22, &quot;column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;datatype&quot;: &quot;composite&quot;, &quot;is_custom&quot;: true, &quot;kind&quot;: &quot;field&quot;, &quot;#extra#&quot;: null, &quot;is_category&quot;: false, &quot;display&quot;: {&quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;description&quot;: &quot;&quot;, &quot;use_decorations&quot;: 0, &quot;make_category&quot;: false, &quot;contains_html&quot;: false}, &quot;#value#&quot;: &quot;9781588904027&quot;, &quot;is_multiple&quot;: null, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_editable&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
