<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">1478</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">3445ecd8-beca-425b-84c3-ed1d641d77ae</dc:identifier>
        <dc:title>Skeletal Imaging: Atlas of the Spine and Extremities</dc:title>
        <dc:creator opf:file-as="Taylor, John A. M. &amp; Hughes, Tudor H. &amp; Resnick, Donald L." opf:role="aut">John A. M. Taylor</dc:creator>
        <dc:creator opf:file-as="Taylor, John A. M. &amp; Hughes, Tudor H. &amp; Resnick, Donald L." opf:role="aut">Tudor H. Hughes</dc:creator>
        <dc:creator opf:file-as="Taylor, John A. M. &amp; Hughes, Tudor H. &amp; Resnick, Donald L." opf:role="aut">Donald L. Resnick</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.7.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2009-12-22T23:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;Use this atlas to accurately interpret images of musculoskeletal disorders! Taylor, Hughes, and Resnick’s &lt;strong&gt;Skeletal Imaging: Atlas of the Spine and Extremities, 2nd Edition&lt;/strong&gt; covers each anatomic region separately, so common disorders are shown within the context of each region. This allows you to examine and compare images for a variety of different disorders. A separate chapter is devoted to each body region, with coverage of normal developmental anatomy, developmental anomalies and normal variations, and how to avoid a misdiagnosis by differentiating between disorders that appear to be similar. All of the most frequently encountered musculoskeletal conditions are included, from physical injuries to tumors to infectious diseases.&lt;/p&gt;&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;Over 2,100 images&lt;/strong&gt; include radiographs, radionuclide studies, CT scans, and MR images, illustrating pathologies and comparing them with other disorders in the same region.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;Organization by anatomic region&lt;/strong&gt; addresses common afflictions for each region in separate chapters, so you can see how a particular region looks when affected by one condition as compared to its appearance with other conditions.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;Coverage of each body region&lt;/strong&gt; includes normal developmental anatomy, fractures, deformities, dislocations, infections, hematologic disorders, and more. &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;&lt;em&gt;Normal Developmental Anatomy&lt;/em&gt; sections&lt;/strong&gt; open each chapter, describing important developmental landmarks in various regions of the body from birth to skeletal maturity.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;Practical tables&lt;/strong&gt; provide a quick reference to essential information, including normal developmental anatomic milestones, developmental anomalies, common presentations and symptoms of diseases, and much more. &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;400 new and replacement images&lt;/strong&gt; are added to the book, showing a wider variety of pathologies. &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;More MR imaging&lt;/strong&gt; is added to each chapter.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;Up-to-date research&lt;/strong&gt; includes the latest on scientific advances in imaging.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;strong&gt;References&lt;/strong&gt; are completely updated with new information and evidence.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;&lt;p class="description"&gt;

&lt;/p&gt;&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Saunders</dc:publisher>
        <dc:identifier opf:scheme="AMAZON">1416056238</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">M62EtQAACAAJ</dc:identifier>
        <dc:identifier opf:scheme="ISBN">9781416056232</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>Allied Health Services</dc:subject>
        <dc:subject>Imaging Technologies</dc:subject>
        <dc:subject>Chiropractic</dc:subject>
        <dc:subject>Science</dc:subject>
        <dc:subject>Life Sciences</dc:subject>
        <dc:subject>Anatomy &amp; Physiology</dc:subject>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Donald L. Resnick&quot;: &quot;&quot;, &quot;Tudor H. Hughes&quot;: &quot;&quot;, &quot;John A. M. Taylor&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="10" name="calibre:rating"/>
        <meta content="2017-08-10T21:44:32+00:00" name="calibre:timestamp"/>
        <meta content="Skeletal Imaging: Atlas of the Spine and Extremities" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;datatype&quot;: &quot;composite&quot;, &quot;colnum&quot;: 1, &quot;link_column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;is_multiple2&quot;: {}, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_csp&quot;: false, &quot;kind&quot;: &quot;field&quot;, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;#value#&quot;: &quot;9781416056232&quot;, &quot;display&quot;: {&quot;contains_html&quot;: false, &quot;use_decorations&quot;: 0, &quot;description&quot;: &quot;&quot;, &quot;make_category&quot;: false, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;}, &quot;is_category&quot;: false, &quot;rec_index&quot;: 22, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;column&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;label&quot;: &quot;isbn&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
