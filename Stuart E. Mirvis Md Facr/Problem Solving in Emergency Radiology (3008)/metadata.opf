<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3008</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">3f42b427-36a6-4931-b95e-d6b280d59ed5</dc:identifier>
        <dc:title>Problem Solving in Emergency Radiology</dc:title>
        <dc:creator opf:file-as="Facr, Stuart E. Mirvis Md &amp; Soto, Jorge A Md &amp; Shanmuganathan, Kathirkamanathan Md &amp; Yu, Joseph Md &amp; Kubal, Wayne S Md" opf:role="aut">Stuart E. Mirvis Md Facr</dc:creator>
        <dc:creator opf:file-as="Facr, Stuart E. Mirvis Md &amp; Soto, Jorge A Md &amp; Shanmuganathan, Kathirkamanathan Md &amp; Yu, Joseph Md &amp; Kubal, Wayne S Md" opf:role="aut">Jorge A Soto Md</dc:creator>
        <dc:creator opf:file-as="Facr, Stuart E. Mirvis Md &amp; Soto, Jorge A Md &amp; Shanmuganathan, Kathirkamanathan Md &amp; Yu, Joseph Md &amp; Kubal, Wayne S Md" opf:role="aut">Kathirkamanathan Shanmuganathan Md</dc:creator>
        <dc:creator opf:file-as="Facr, Stuart E. Mirvis Md &amp; Soto, Jorge A Md &amp; Shanmuganathan, Kathirkamanathan Md &amp; Yu, Joseph Md &amp; Kubal, Wayne S Md" opf:role="aut">Joseph Yu Md</dc:creator>
        <dc:creator opf:file-as="Facr, Stuart E. Mirvis Md &amp; Soto, Jorge A Md &amp; Shanmuganathan, Kathirkamanathan Md &amp; Yu, Joseph Md &amp; Kubal, Wayne S Md" opf:role="aut">Wayne S Kubal Md</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.7.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2014-10-16T22:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;Optimize diagnostic accuracy in the emergency department with &lt;strong&gt;&lt;em&gt;Problem Solving in Radiology: Emergency Radiology&lt;/em&gt;&lt;/strong&gt;, a new addition to the popular&lt;em&gt; &lt;/em&gt;*Problem Solving in Radiology &lt;strong&gt;&lt;em&gt;series. Published in association with the &lt;/em&gt;*American Society of Emergency Radiology&lt;/strong&gt;, the medical reference book is designed to help experienced radiologists, residents, or emergency medicine practitioners accurately address problematic conditions and reach the most accurate diagnosis.&lt;/p&gt;
&lt;p&gt;*"This book will have broad appeal to many audiences. It will prove an invaluable resource to any practicing radiologist providing coverage of emergency department imaging, regardless of whether or not the radiologist self-identifies as an emergency radiologist."&lt;strong&gt;*Foreword by:&lt;/strong&gt; Stephen Ledbetter, Chief of Radiology Brigham and Women’s Faulkner Hospital, May 2015&lt;/p&gt;
&lt;hr&gt;
&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Access problem-oriented content&lt;/strong&gt; that helps you quickly and accurately diagnose patients.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Focus on the core knowledge needed for successful results&lt;/strong&gt; with templated, concise chapters containing both traditional and unusual presentations of pathology. Each chapter will include: &lt;strong&gt;Typical Presentation&lt;/strong&gt;; &lt;strong&gt;Variants&lt;/strong&gt;; &lt;strong&gt;Mimickers&lt;/strong&gt; (what looks like this pathology, but isn’t); and &lt;strong&gt;Pitfalls&lt;/strong&gt; (how a diagnosis can be missed and how to avoid it).&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Stay up to date on today's hot topics in radiology,&lt;/strong&gt; including radiation concerns when using total body CT for trauma assessment; trauma in the pregnant patient; imaging pediatric craniocerebral trauma; and penetrating trauma to the torso and chest.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Access the full text online &lt;/strong&gt;at &lt;strong&gt;Expert Consult.&lt;/strong&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Saunders</dc:publisher>
        <dc:identifier opf:scheme="AMAZON">145575417X</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">L2XlngEACAAJ</dc:identifier>
        <dc:identifier opf:scheme="ISBN">9781455754175</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>Radiology; Radiotherapy &amp; Nuclear Medicine</dc:subject>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Jorge A Soto Md&quot;: &quot;&quot;, &quot;Wayne S Kubal Md&quot;: &quot;&quot;, &quot;Joseph Yu Md&quot;: &quot;&quot;, &quot;Stuart E. Mirvis Md Facr&quot;: &quot;&quot;, &quot;Kathirkamanathan Shanmuganathan Md&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="Problem Solving in Emergency Radiology Series" name="calibre:series"/>
        <meta content="1" name="calibre:series_index"/>
        <meta content="2017-08-20T17:28:48+00:00" name="calibre:timestamp"/>
        <meta content="Problem Solving in Emergency Radiology" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;datatype&quot;: &quot;composite&quot;, &quot;colnum&quot;: 1, &quot;link_column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;is_multiple2&quot;: {}, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_csp&quot;: false, &quot;kind&quot;: &quot;field&quot;, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;#value#&quot;: &quot;9781455754175&quot;, &quot;display&quot;: {&quot;contains_html&quot;: false, &quot;use_decorations&quot;: 0, &quot;description&quot;: &quot;&quot;, &quot;make_category&quot;: false, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;}, &quot;is_category&quot;: false, &quot;rec_index&quot;: 22, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;column&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;label&quot;: &quot;isbn&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
