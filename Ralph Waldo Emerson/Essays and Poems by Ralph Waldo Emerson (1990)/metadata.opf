<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">1990</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">8b3747bf-b9ab-48dd-a0dc-aedba28584bf</dc:identifier>
        <dc:title>Essays and Poems by Ralph Waldo Emerson</dc:title>
        <dc:creator opf:file-as="Emerson, Ralph Waldo" opf:role="aut">Ralph Waldo Emerson</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.6.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2010-07-10T07:00:00+00:00</dc:date>
        <dc:description>SUMMARY: Essays and Poems, by Ralph Waldo Emerson, is part of the Barnes &amp; Noble Classics series, which offers quality editions at affordable prices to the student and the general reader, including new scholarship, thoughtful design, and pages of carefully crafted extras. Here are some of the remarkable features of Barnes &amp; Noble Classics: New introductions commissioned from today's top writers and scholars Biographies of the authors Chronologies of contemporary historical, biographical, and cultural events Footnotes and endnotes Selective discussions of imitations, parodies, poems, books, plays, paintings, operas, statuary, and films inspired by the work Comments by other famous authors Study questions to challenge the reader's viewpoints and expectations Bibliographies for further reading Indices &amp; Glossaries, when appropriateAll editions are beautifully designed and are printed to superior specifications; some include illustrations of historical interest. Barnes &amp; Noble Classics pulls together a constellation of influences—biographical, historical, and literary—to enrich each reader's understanding of these enduring works. As an adolescent America searched for its unique identity among the nations of the world, a number of thinkers and writers emerged eager to share their vision of what the American character could be. Among their leaders was Ralph Waldo Emerson, whose essays, lectures, and poems defined the American transcendentalist movement, though he himself disliked the term. Emerson advocates a rejection of fear-driven conformity, a total independence of thought and spirit, and a life lived in harmony with nature. He believes that Truth lies within each individual, for each is part of a greater whole, a universal “over-soul” through which we transcend the merely mortal. Emerson was extremely prolific throughout his life; his collected writings fill forty volumes. This edition contains his major works, including Nature, the essays “Self-Reliance,” “The American Scholar,” “The Over-Soul,” “Circles,” “The Poet,” and “Experience,”, and such important poems as “The Rhodora,” “Uriel,” “The Humble-Bee,” “Earth-Song,” “Give All to Love,” and the well-loved “Concord Hymn.” Includes a comprehensive glossary of names. Peter Norberg has been Assistant Professor of English at Saint Joseph’s University in Philadelphia since 1997. A specialist in New England transcendentalism and the history of the antebellum period, he also has published on Herman Melville’s poetry. He currently is writing a history of Emerson’s career as a public lecturer.</dc:description>
        <dc:publisher>Barnes&amp;Noble</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781411432123</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>General</dc:subject>
        <dc:subject>American poetry</dc:subject>
        <dc:subject>American essays</dc:subject>
        <dc:subject>Criticism</dc:subject>
        <dc:subject>Literary Criticism</dc:subject>
        <dc:subject>Literature - Classics</dc:subject>
        <dc:subject>Prose: non-fiction</dc:subject>
        <dc:subject>Poetry Texts &amp; Poetry Anthologies</dc:subject>
        <dc:subject>American - General</dc:subject>
        <dc:subject>Literary Collections</dc:subject>
        <dc:subject>American</dc:subject>
        <dc:subject>Poetry</dc:subject>
        <dc:subject>Fiction</dc:subject>
        <dc:subject>Books &amp; Reading</dc:subject>
        <dc:subject>Classics</dc:subject>
        <meta content="{&quot;Ralph Waldo Emerson&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-08-19T20:03:28+00:00" name="calibre:timestamp"/>
        <meta content="Essays and Poems by Ralph Waldo Emerson" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;is_csp&quot;: false, &quot;name&quot;: &quot;ISBN&quot;, &quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;#value#&quot;: &quot;9781411432123&quot;, &quot;is_multiple2&quot;: {}, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;is_custom&quot;: true, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;composite&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;display&quot;: {&quot;description&quot;: &quot;&quot;, &quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false, &quot;use_decorations&quot;: 0, &quot;contains_html&quot;: false}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;category_sort&quot;: &quot;value&quot;, &quot;#extra#&quot;: null, &quot;colnum&quot;: 1, &quot;is_editable&quot;: true}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
