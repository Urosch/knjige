<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3472</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">aa7901aa-9d31-4436-9be8-e8a6d6ebba63</dc:identifier>
        <dc:title>Color Atlas of Cerebral Revascularization: Anatomy, Techniques, Clinical Cases</dc:title>
        <dc:creator opf:file-as="Spetzler, Robert Friedrich &amp; Albert L. Rhoton, Jr. M. D. &amp; Masatou Kawashima, M. D. Ph.d. &amp; Nakaji, Peter" opf:role="aut">Robert Friedrich Spetzler</dc:creator>
        <dc:creator opf:file-as="Spetzler, Robert Friedrich &amp; Albert L. Rhoton, Jr. M. D. &amp; Masatou Kawashima, M. D. Ph.d. &amp; Nakaji, Peter" opf:role="aut">Albert L. Rhoton, Jr. M. D.</dc:creator>
        <dc:creator opf:file-as="Spetzler, Robert Friedrich &amp; Albert L. Rhoton, Jr. M. D. &amp; Masatou Kawashima, M. D. Ph.d. &amp; Nakaji, Peter" opf:role="aut">Masatou Kawashima, M. D. Ph.d.</dc:creator>
        <dc:creator opf:file-as="Spetzler, Robert Friedrich &amp; Albert L. Rhoton, Jr. M. D. &amp; Masatou Kawashima, M. D. Ph.d. &amp; Nakaji, Peter" opf:role="aut">Peter Nakaji</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.10.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2013-02-26T23:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;&lt;strong&gt;Gold winner in 2014 IBPA Ben Franklin Awards!&lt;/strong&gt;   &lt;/p&gt;
&lt;p&gt;"...&lt;em&gt; the images are second-to-none in their ability to present the subject material.&lt;br&gt;
... The authors have made this atlas an efficient and informative read. It is this pairing of operative photographs with high-quality illustrations that raises this text to a level superior to that of its competitors&lt;/em&gt;." -- &lt;strong&gt;American Journal of Neuroradiology&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;A highly-anticipated addition to Thieme's classic color atlas collection, &lt;em&gt;Color Atlas of Cerebral Revascularization&lt;/em&gt; focuses on cerebral bypass techniques pioneered by leading surgeons at the world-renowned Barrow Neurological Institute in Phoenix, Arizona. Each procedure is presented with intraoperative photographs and exquisite anatomical illustrations to help surgeons master the complex microsurgical anatomy and subtle surgical technique used in managing the potential onset and condition of stroke and other causes of cerebral ischemia.&lt;/p&gt;
&lt;p&gt;Key Features:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;Side-by-side photo and illustration format aids in interpretation of intricate surgical procedures   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;More than 1300 figures elucidate clinical cases from the Barrow Neurological Institute and other centers of neurosurgical excellence   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;A DVD, featuring more than 30 related surgical cases and narrated by the authors, is included with the book   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Cases illustrate how to successfully achieve revascularization for conditions such as moyamoya disease, recurrent aneurysms after endovascular treatment, giant aneurysms, vertebral artery insufficiency, and severe stenosis   &lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;The vascular anatomy related to each bypass technique is illustrated and described in the sections showcasing the clinical cases treated by the technique&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;This comprehensive atlas is an ideal reference for practicing neurosurgeons, neurosurgical residents, and interventional neuroradiologists, and it will be a relevant volume in their medical library for years to come.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Thieme</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781604068221</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">lmulMQEACAAJ</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">1604068221</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>Neuroscience</dc:subject>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Robert Friedrich Spetzler&quot;: &quot;&quot;, &quot;Peter Nakaji&quot;: &quot;&quot;, &quot;Masatou Kawashima, M. D. Ph.d.&quot;: &quot;&quot;, &quot;Albert L. Rhoton, Jr. M. D.&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="Thieme Flexibooks" name="calibre:series"/>
        <meta content="1" name="calibre:series_index"/>
        <meta content="10" name="calibre:rating"/>
        <meta content="2017-10-25T18:55:36.802255+00:00" name="calibre:timestamp"/>
        <meta content="Color Atlas of Cerebral Revascularization: Anatomy, Techniques, Clinical Cases" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;label&quot;: &quot;isbn&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;search_terms&quot;: [&quot;#isbn&quot;], &quot;rec_index&quot;: 22, &quot;column&quot;: &quot;value&quot;, &quot;name&quot;: &quot;ISBN&quot;, &quot;datatype&quot;: &quot;composite&quot;, &quot;is_custom&quot;: true, &quot;kind&quot;: &quot;field&quot;, &quot;#extra#&quot;: null, &quot;is_category&quot;: false, &quot;display&quot;: {&quot;composite_sort&quot;: &quot;text&quot;, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;description&quot;: &quot;&quot;, &quot;use_decorations&quot;: 0, &quot;make_category&quot;: false, &quot;contains_html&quot;: false}, &quot;#value#&quot;: &quot;9781604068221&quot;, &quot;is_multiple&quot;: null, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_editable&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
