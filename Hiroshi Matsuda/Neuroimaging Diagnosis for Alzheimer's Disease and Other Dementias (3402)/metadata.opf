<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3402</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">881ba772-7ade-4eb2-90e1-b3985abaf0b2</dc:identifier>
        <dc:title>Neuroimaging Diagnosis for Alzheimer's Disease and Other Dementias</dc:title>
        <dc:creator opf:file-as="Matsuda, Hiroshi &amp; Asada, Takashi &amp; Tokumaru, Aya Midori" opf:role="aut">Hiroshi Matsuda</dc:creator>
        <dc:creator opf:file-as="Matsuda, Hiroshi &amp; Asada, Takashi &amp; Tokumaru, Aya Midori" opf:role="aut">Takashi Asada</dc:creator>
        <dc:creator opf:file-as="Matsuda, Hiroshi &amp; Asada, Takashi &amp; Tokumaru, Aya Midori" opf:role="aut">Aya Midori Tokumaru</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.9.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2016-01-04T23:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p&gt;This book describes the latest modalities such as tau PET imaging for diagnosis of Alzheimer’s disease and other dementias, and also provides information on handling and analyzing imaging data that is not found in other books. In addition, it introduces routine imaging studies in the management of dementia in Japan. The prevalence of dementia has increased over the past few decades, either because of greater awareness and more accurate diagnosis, or because increased longevity has created a larger population of the elderly, the age group most commonly affected. Although only clinical assessment can lead to a diagnosis of dementia, neuroimaging in dementia is recommended by most clinical guidelines, and its adjunct role has traditionally been to exclude a mass lesion rather than to support a specific diagnosis. Neuroimaging may be also helpful for developing new strategies to achieve diagnoses as early as possible for therapies aimed at slowing the progression of neurodegenerative diseases manifesting dementia. Under these conditions, all clinicians and researchers who are involved in neuroimaging for dementia should decide which patients to scan, when imaging patients is most useful, which modality to use, how to handle imaging data from many institutions, and which analytical tool to use. This edition comprises contributions from leading Japanese experts in their fields.&lt;/p&gt;
&lt;p&gt;**&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>Springer</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9784431551324</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">ZXwKrgEACAAJ</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">4431551328</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Medical</dc:subject>
        <dc:subject>Biochemistry</dc:subject>
        <dc:subject>Diagnostic Imaging</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Radiology; Radiotherapy &amp; Nuclear Medicine</dc:subject>
        <dc:subject>Neurology</dc:subject>
        <dc:subject>Allied Health Services</dc:subject>
        <dc:subject>Imaging Technologies</dc:subject>
        <dc:subject>Clinical Medicine</dc:subject>
        <dc:subject>Radiologija</dc:subject>
        <meta content="{&quot;Hiroshi Matsuda&quot;: &quot;&quot;, &quot;Aya Midori Tokumaru&quot;: &quot;&quot;, &quot;Takashi Asada&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2017-10-15T12:50:35.315397+00:00" name="calibre:timestamp"/>
        <meta content="Neuroimaging Diagnosis for Alzheimer's Disease and Other Dementias" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#isbn" content="{&quot;name&quot;: &quot;ISBN&quot;, &quot;column&quot;: &quot;value&quot;, &quot;link_column&quot;: &quot;value&quot;, &quot;kind&quot;: &quot;field&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;display&quot;: {&quot;use_decorations&quot;: 0, &quot;composite_sort&quot;: &quot;text&quot;, &quot;description&quot;: &quot;&quot;, &quot;contains_html&quot;: false, &quot;composite_template&quot;: &quot;{identifiers:select(isbn)}&quot;, &quot;make_category&quot;: false}, &quot;datatype&quot;: &quot;composite&quot;, &quot;category_sort&quot;: &quot;value&quot;, &quot;is_custom&quot;: true, &quot;label&quot;: &quot;isbn&quot;, &quot;rec_index&quot;: 22, &quot;#extra#&quot;: null, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_category&quot;: false, &quot;#value#&quot;: &quot;9784431551324&quot;, &quot;is_editable&quot;: true, &quot;search_terms&quot;: [&quot;#isbn&quot;]}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
